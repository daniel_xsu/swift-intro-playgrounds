// Closures

import Foundation

// *************************************************************************

// Closures are self-contained blocks of functionality that can 
// be passed around and used in your code.  
// 
// Closures can capture and store references to any constants and 
// variables from the context in which they are defined. This is 
// known as closing over those constants and variables, hence 
// the name “closures".
//
// Excerpt From: Apple Inc. “The Swift Programming Language.”
// Examples below also from Closures chapter in book.

// *************************************************************************

let names = [ "Chris", "Alex", "Ewa", "Barry", "Daniella" ]

// Simple closure as a global function.

func backwards(s1: String, s2: String) -> Bool {
    return s1 > s2
}
var reversed = sort(names, backwards)

// Inline closures, using closure expression syntax.
//
// Closure Expression Syntax
//     { ( <parameters> ) -> <returntype> in
//             <statements>
//     }

reversed = sort(names, { (s1: String, s2: String) -> Bool in
    return s1 > s2
})

// *************************************************************************

// Swift can infer the type of the closure; "(String, String) -> Bool" in 
// this case.  

reversed = sort(names, { s1, s2 in return s1 > s2 } )

// *************************************************************************

// Implicit return when closure is only a single expression

reversed = sort(names, { s1, s2 in s1 > s2 } )

// *************************************************************************

// Short-hand argument names.

reversed = sort(names, { $0 > $1 } )

// *************************************************************************

// Crazy, super short version.  Uses the > operator from the String type.
// This operator is a function with the type "(String, String) -> Bool". This
// exactly matches the function type for the closure, so it can be passed
// as the closure.

reversed = sort(names, >)

// *************************************************************************

// Trailing closures - Can move closure expression outside parentheses. 
// These two examples mean the same thing, but the syntax is nicer for
// the second one. It is using the trailing closure.

reversed = sort(names, { $0 > $1 } )

reversed = sort(names) { $0 > $1 }

// *************************************************************************

// Capturing values - closure "incrementor" captures the runningTotal
// variable from the surrounding function.

func makeIncrementor(forIncrement amount: Int) -> () -> Int {
    var runningTotal = 0
    func incrementor() -> Int {
        runningTotal += amount
        return runningTotal
    }
    return incrementor
}

let incrementByTen = makeIncrementor(forIncrement: 10)

incrementByTen()
incrementByTen()

let incrementBySeven = makeIncrementor(forIncrement: 7)

incrementBySeven()
incrementByTen()

// *************************************************************************

// Closures are reference types
let anotherIncrementor = incrementByTen

anotherIncrementor()

// *************************************************************************
