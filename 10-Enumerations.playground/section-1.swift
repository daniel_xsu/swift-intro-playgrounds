// Enumerations

// Defines a common type for a group of related values.
// Name should start with capital letter.
// Use singular rathar than plural names.

// *************************************************************************

// Simple forms

enum CompassPoint {
    case North
    case South
    case East
    case West
}

enum Planet {
    case Mercury, Venus, Earth
    case Mars, Jupiter, Saturn
    case Uranus, Neptune
}

// *************************************************************************

var directionToHead = CompassPoint.West

directionToHead = .East

// *************************************************************************

// Switch on enumeration values

switch directionToHead {
case .North:
    println("Heading North!")
case .East:
    println("Heading East.")
default:
    println("Going another way")
}

// *************************************************************************

// Associated Values

enum BarCode {
    case UPCA(Int, Int, Int)
    case QRCode(String)
}

var productBarCode = BarCode.UPCA(8, 85909_51226, 3)
productBarCode = .QRCode("ABCDEFGHIJKLMNOP")

// *************************************************************************

switch productBarCode {
case .UPCA(let numberSystem, let identifier, let check):
    println("UPC-A with value of \(numberSystem), \(identifier), \(check).")
case .QRCode(let productCode):
    println("QR code with value of \(productCode)")
}

// *************************************************************************

// Raw values

enum ASCIIControlCharacters: Character {
    case Tab = "\t"
    case LineFeed = "\n"
    case CarriageReturn = "\r"
}

enum MonthOfYear: Int {
    case January = 1
    case February
    case March
    case April
    case May
    case June
    case July
    case August
    case September
    case October
    case November
    case December
}

var month = MonthOfYear.March
month.toRaw()

if let possibleMonth = MonthOfYear.fromRaw(12) {
    month = possibleMonth
}
else {
    println("Not a valid month value")
}

// *************************************************************************
