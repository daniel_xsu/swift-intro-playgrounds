// Playground - noun: a place where people can play

import Cocoa

// *****************************************************************************
// S T R U C T U R E S
// *****************************************************************************

// *************************************************
// Define a structure holding a 2-dimensional point.
// *************************************************
struct Point {
    var x = 0.0
    var y = 0.0
}

// *************************************************************
// Create an instance of the structure using initializer syntax.
// *************************************************************

let origin = Point()

// Print values of structure properties.
println("Point at (\(origin.x), \(origin.y))")

// *******************************************
// Memberwise Initializers for Structure Types
// *******************************************

let point = Point(x: 5.0, y: 10.0)

// ***************************
// Structures are value types.
// ***************************

var o1 = origin
var o2 = o1

o1.x = 5.0

o1
o2

// ******************
// Nesting Structures
// ******************

struct Size {
    var width = 0.0
    var height = 0.0
}

struct Rectangle {
    var origin = Point()
    var size = Size()
}

var square1 = Rectangle(origin: Point(x: 0.0, y: 0.0), size: Size(width: 10.0, height: 10.0))

// *****************************************************************************
// C L A S S E S
// *****************************************************************************

// *************************************
// Define a class that describes a shape
// *************************************

class Shape {
    var x = 0
    var y = 0
    var name: String?
}

// ********************************************
// Create an instance using initializer syntax.
// ********************************************

let shape = Shape()
println("shape at (\(shape.x), \(shape.y)) with name '\(shape.name)'")

// No memberwise initializer provided for a class!

// ***************************
// Classes are Reference Types
// ***************************

var s1 = shape
var s2 = s1

s1.x = 10

s1
s2

// Maybe unexpected, but value of property in constant was also changed!
shape

// ******************
// Identity Operators
// ******************

// Identical to (===)
// Not identitical to (!==)

s1 === s2
s2 === shape

let s3 = Shape()
s3.x = 10

s2 === s3

// **********************
// Lazy Stored Properties
// **********************

class ComputeTask {
    @lazy var description = String()
    var name = "ComputeTask"
}

var ct = ComputeTask()
ct
ct.description = "a task"
ct

// ************************************
// Compute Properties (class or struct)
// ************************************

struct Rect {
    var origin = Point()
    var size = Size()
    var center: Point {
        get {
            let centerX = origin.x + (size.width / 2)
            let centerY = origin.y + (size.height / 2)
            return Point(x: centerX, y: centerY)
        }

        // "newValue" is default name for passed value
        // Could replace with named parameter if desired.
    
        set /*(newCenter)*/ {
            origin.x = newValue.x - (size.width / 2)
            origin.y = newValue.y - (size.height / 2)
        }
    }
}

var square2 = Rect(origin: Point(x: 0.0, y: 0.0), size: Size(width: 10.0, height: 10.0))
let initialSquareCenter = square2.center
square2.center = Point(x: 15.0, y: 15.0)
square2

// Read-only Computed Properties
// Must declare as 'var', not 'let'  They change when computed!

struct Square {
    var size = Size()
    var area: Double { return size.width * size.height }
}

let s = Square(size: Size(width: 10.0, height: 10.0))
s.area

// s.area = 100

// ******************
// Property Observers
// ******************

class StepCounter {
    var totalSteps: Int = 0 {
        willSet(newTotalSteps) {
            // New value passed in using named parameter
            println("About to set totalSteps to \(newTotalSteps)")
        }
        didSet {
            // "oldValue" is default name for parameter
            if totalSteps > oldValue {
                println("added \(totalSteps - oldValue) steps")
            }
        }
    }
}

let stepCounter = StepCounter()
stepCounter.totalSteps = 200

stepCounter.totalSteps = 360

stepCounter.totalSteps = 896

// ***************
// Type Properties
// ***************

struct CountedPoint {
    static var numberOfInstances: Int = 0
    var x = 0.0
    var y = 0.0
}

println("There are \(CountedPoint.numberOfInstances) instances of CountedPoint right now.")

// Not yet supported, but this is how they would be done.
//class CountedObject {
//    class var numberOfInstances: Int = 0
//}

//println("There are \(CountedObject.numberOfInstances) instances right now.")

// *****************************************************************************
// M E T H O D S
// *****************************************************************************

class Counter {
    var count = 0
    func increment() {
        count++
    }
    func incrementBy(amount: Int) {
        incrementBy(amount, numberOfTimes: 1)
    }
    func incrementBy(amount: Int, numberOfTimes: Int) {
        count += amount * numberOfTimes
    }
    func reset() {
        // The use of self is optional here.
        self.count = 0
    }
    func resetToCount(#count: Int) {
        // The use of self is required here to disambiguate.
        self.count = count
    }
}

let counter = Counter()
counter.increment()
counter.incrementBy(5)
counter.reset()
counter.resetToCount(count: 10)

// ************************************************************
// Method parameter naming (local and external parameter names)
// ************************************************************

func ex1_incrementBy(amount: Int, numberOfTimes: Int) {
    // Does nothing...
}

func ex2_incrementBy(amount: Int, #numberOfTimes: Int) {
    // Does nothing...
}

ex1_incrementBy(10, 10)
ex2_incrementBy(10, numberOfTimes: 10)

// Notice that method definition did not explicitly provide an external
// parameter name for "numberOfTimes", but Swift provided one.
counter.incrementBy(10, numberOfTimes: 10)


// Next Week - More About Classes & Structures
//
// - Opting into mutating behavior on value types.
// - Type Methods
// - Subscripts
// - Inheritance (base classes, subclassing, overriding)
// - Initialization














