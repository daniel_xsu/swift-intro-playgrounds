// Functions - Part 2

import Foundation

// *************************************************************************

// Simple "void" function - takes no parameters, returns nothing

func sayHi() {
    println("Hi")
}

sayHi()

// *************************************************************************

// Function taking two parameters, printing result of adding parameters.

func printAplusB(a: Int, b: Int) {
    println("\(a) + \(b) = \(a+b)")
}

printAplusB(10, 20)

// *************************************************************************

// Function returning the answer as a string.
//    -> is called the "return arrow"

func resultOfAplusB(a: Int, b: Int) -> String {
    return "\(a) + \(b) = \(a+b)"
}

// Try option+click on "result".  Xcode shows you the type info!
let result = resultOfAplusB(30, 40)

// *************************************************************************

// Returning a tuple

func doHTTPGet(urlString: String) -> (statusCode: Int, statusMessage: String) {
    // Imagine we are doing an actual HTTP request here.
    
    return (200, "OK")
}

let getResult = doHTTPGet("http://twitter.com")
println("Code - \(getResult.statusCode)")
println("Message - \(getResult.statusMessage)")

// *************************************************************************

// You must return a value from each control path in a function!

func missingReturn(flag: Bool) -> Int {
    if flag == true {
        return 1
    }
    else {
        // Comment out to see error.
        return 0
        // error: missing return in a function expected to return 'Int'
    }
}

// *********************
// START HERE FOR PART 2
// *********************

// *************************************************************************

// External parameter names (named parameters).  If provided, external
// parameter names must always be used when calling function.

func distanceBetweenPoints(X1 x1: Double, Y1 y1: Double, X2 x2:Double, Y2 y2:Double) -> Double {
    let dx = x2 - x1
    let dy = y2 - y1
    return sqrt(dx * dx + dy * dy)
}

let distance = distanceBetweenPoints(X1: 0.0, Y1: 0.0, X2: 10.0, Y2: 0.0)

// *************************************************************************

// Short-hand for external names (use when internal name is suitable for use
// as external name too.

func lineLength(#x1: Double, #y1: Double, #x2: Double, #y2: Double) -> Double {
    let dx = x2 - x1
    let dy = y2 - y1
    return sqrt(dx * dx + dy * dy)
}

let length = lineLength(x1: 0.0, y1: 0.0, x2: 10.0, y2: 0.0)

// *************************************************************************

// Default parameter values

func concatenateStrings(s1: String, s2: String, separator: String = " ") -> String {
    return s1 + separator + s2
}

concatenateStrings("Bennett", "Smith")

// Note: parameter with default value always has external name!
concatenateStrings("Hello", "World", separator: ", ")

// *************************************************************************

// Variadic paramters (variable length array of parameters, all of same type!)

func addNumbers(numbers: Double ...) -> Double {
    var sum = 0.0
    for number in numbers {
        sum += number
    }
    return sum
}

let total = addNumbers(0.99, 1.21, 0.30, 2.50)

// *************************************************************************

// Constant vs. variable parameters.
// Variable parameters can be changed within the function, but their
// values do not live beyond the scope of the function.

func applyOffset_Broken(number: Int, offset: Int) {
    // Uncomment to see error.
    // number = number + offset
    // error: cannot assign to 'let' value 'number'
}

func applyOffset_Works(var number: Int, offset: Int) {
    number = number + offset
}

var n = 10
let o = 2

applyOffset_Broken(n, o)
n

applyOffset_Works(n, o)
n

// *************************************************************************

// In-Out Parameters
func applyOffset(inout Number n:Int, let Offset o:Int = 5) {
    n = n + o
}

applyOffset(Number: &n)
n

applyOffset(Number: &n, Offset: 25)
n

// *************************************************************************

// Function Types - Every function is actually a Swift type, made up of the
// parameter types and the return type.

// A function that takes no parameters and returns nothing.
func degenerateCase() {
    
}
// Type of this function is "() -> ()"

// More interesting case...

func add(a: Int, b: Int) -> Int {
    return a + b
}

func mul(a: Int, b: Int) -> Int {
    return a * b
}

// Type for both functions is "(Int, Int) -> Int"

// Can store a function in a variable like so:
var mathOp: (Int, Int) -> Int = mul

// Can invoke function using function type like so:
let r1 = mathOp(2, 3)

// And re-assign it, pointing at a different function.
mathOp = add

let r2 = mathOp(2, 3)

// This is very similar to C function pointers!

// *************************************************************************

// Passing function type as a parameter

func performMathOp(mathOp: (Int, Int) -> Int, a: Int, b: Int) {
    let result = mathOp(a, b)
    println("result = \(result)")
}

performMathOp(add, 3, 2)

// *************************************************************************

// Returning function type from a function

func randomMathOp() -> (Int, Int) -> Int {
    let r = arc4random_uniform(1)
    if r == 0 {
        return add
    }
    else {
        return mul
    }
}

mathOp = randomMathOp()
let r3 = mathOp(2, 2)

// *************************************************************************

// Nested functions

func sortNumbers(numbers: Int[]) -> Int[] {
    func compare(n1: Int, n2: Int) -> Bool {
        return n1 < n2
    }
    
    var result = sort(numbers, compare)
    
    return result
}

let numbers = [ 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 ]
let sortedNumbers = sortNumbers(numbers)

// *************************************************************************



